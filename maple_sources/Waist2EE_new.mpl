
# FGM

#=====================================================================
st := time():

fname := "Waist2EE_new":
printf("Generating %s ...\n",cat(fname,".c"));
fd := fopen(cat("../c_sources/",cat(fname,".c")),WRITE):
fprintf(fd,"/* Generated using codegen (%s) */ \n", StringTools:-FormatTime("%Y-%m-%d, %T")):
fclose(fd):
#=====================================================================

# ===================================================================================
# for convenience we use a homogeneous matrix to store the posture of the Waist
# LL is used as an input parameter in the C functiona
# ===================================================================================
LL := vector(16): # Left leg transformation matrix <-- this is the support leg

q[30] := LL[1]: q[33] := LL[5]: q[36] :=  LL[9]: q[27] := LL[13]:
q[31] := LL[2]: q[34] := LL[6]: q[37] := LL[10]: q[28] := LL[14]:
q[32] := LL[3]: q[35] := LL[7]: q[38] := LL[11]: q[29] := LL[15]:
#        LL[4]:          LL[8]:          LL[12]:          LL[16]:
# ===================================================================================

JointFrames := FramesWorld(Waist):

T := EEWorld(JointFrames,RLeg):
T := convert(transpose(T),vector):
Waist2RLeg := makeproc(T,[q::array(1..n),LL::array(1..linalg:-vectdim(LL))]):
C(Waist2RLeg, optimized, filename = cat("../c_sources/",cat(fname,".c")));

T := EEWorld(JointFrames,LLeg):
T := convert(transpose(T),vector):
Waist2LLeg := makeproc(T,[q::array(1..n),LL::array(1..linalg:-vectdim(LL))]):
C(Waist2LLeg, optimized, filename = cat("../c_sources/",cat(fname,".c")));

T := EEWorld(JointFrames,LHand):
T := convert(transpose(T),vector):
Waist2LHand := makeproc(T,[q::array(1..n),LL::array(1..linalg:-vectdim(LL))]):
C(Waist2LHand, optimized, filename = cat("../c_sources/",cat(fname,".c")));

T := EEWorld(JointFrames,RHand):
T := convert(transpose(T),vector):
Waist2RHand := makeproc(T,[q::array(1..n),LL::array(1..linalg:-vectdim(LL))]):
C(Waist2RHand, optimized, filename = cat("../c_sources/",cat(fname,".c")));

T := EEWorld(JointFrames,Head):
T := convert(transpose(T),vector):
Waist2Head := makeproc(T,[q::array(1..n),LL::array(1..linalg:-vectdim(LL))]):
C(Waist2Head, optimized, filename = cat("../c_sources/",cat(fname,".c")));

T := EEWorld(JointFrames,Torso):
T := convert(transpose(T),vector):
Waist2Torso := makeproc(T,[q::array(1..n),LL::array(1..linalg:-vectdim(LL))]):
C(Waist2Torso, optimized, filename = cat("../c_sources/",cat(fname,".c")));

CM := SystemCoM(JointFrames):
Waist2CoM := makeproc(CM,[q::array(1..n),LL::array(1..linalg:-vectdim(LL))]):
C(Waist2CoM, optimized, filename = cat("../c_sources/",cat(fname,".c")));

#---------------------------------------------------------------------
;
