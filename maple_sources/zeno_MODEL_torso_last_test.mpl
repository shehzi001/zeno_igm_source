
# ZENO robot definition with torso joint added as last joint (zeno_MODEL_torso_last)

n := 26: # 26 joints

if RotationsRepresentation = 0 then
    q := vector(n + 6): # 26 joints + 6 DoF for the base link = 32

    # Defines the posture of the Waist frame w.r.t. the world frame.
    WaistFrame := Euler2HomogMatrix(q[27],q[28],q[28],q[29],q[30],q[31]):

elif  RotationsRepresentation = 1 then

    q := vector(n + 3 + 4): # 26 joints + 3 DoF for base link translation + 4 parameters describing its orientation (unit quaternion)

    WaistFrame := Quat2HomogMatrix(q[27],q[28],q[29],q[30],q[31],q[32],q[33]):

elif  RotationsRepresentation = 2 then

    q := vector(n + 3 + 9): # 26 joints + 3 DoF for base link translation + 9 parameters describing its orientation (rotation matrix)

    WaistFrame := Rot2HomogMatrix(q[27],q[28],q[29],[seq(q[i], i=30..38)]):

else

    printf("The variable RotationsRepresentation should be equal to 0 or 1");

end if:


# Using X(alpha)->Y(beta)->Z(gamma) (current axis) Euler angles
#q[27] x
#q[28] y
#q[29] z
#q[30] alpha
#q[31] beta
#q[32] gamma

# Using {S,X,Y,Z} using quaternion
#q[27] x
#q[28] y
#q[29] z
#q[30] S
#q[31] X
#q[32] Y
#q[33] Z

# Using rotation matrix R
#q[27] x
#q[28] y
#q[29] z
#q[30] R(1,1)
#q[31] R(2,1)
#q[32] R(3,1)
#q[33] R(1,2)
#q[34] R(2,2)
#q[35] R(3,2)
#q[36] R(1,3)
#q[37] R(2,3)
#q[38] R(3,3)

#==========================================================================================================================

TorsoOffsetZ     :=  0.01/1000:
NeckOffsetZ      :=  127.7/1000:
ShoulderOffsetX  :=  34.5/1000.0:
ShoulderOffsetY  :=  105.65/1000.0:
ShoulderOffsetZ  :=  85.6/1000.0:
UpperArmLength   :=  111.5/1000.0:
LowerArmLength   :=  82.3/1000.0:
HipRollOffsetX   :=  1.75/1000:
HipRollOffsetY   :=  44.81/1000:
HipRollOffsetZ   :=  53.72/1000:
HipYawOffsetX    :=  23.06/1000:
HipPitchOffsetZ  :=  36.88/1000:
ThighLength      :=  96.77/1000.0:
ShinLength       :=  95.30/1000.0:
FootHeight       :=  81.1/1000.0:
HandOffsetX      :=  0.00/1000.0:
HandOffsetZ      :=  20.0/1000.0:

Joints := []:
Links := []:
EE := []:

#==========================================================================================================================

# Note: Frame 0 is fixed in the Trunk/Waist.
# Left Leg
;
 Joints := [op(Joints), struct('parent',  0, 'r', <-HipRollOffsetX,  HipRollOffsetY,  -HipRollOffsetZ>, 'e', <     0,   Pi/2,    0>)]: #1
;
 Joints := [op(Joints), struct('parent',  1, 'r', <  HipYawOffsetX,               0,    0>, 'e', <     0,  -Pi/2,    0>)]: #2
;
 Joints := [op(Joints), struct('parent',  2, 'r', <              0,               0,    -HipPitchOffsetZ>, 'e', < -Pi/2,   0,    0>)]: #3
;
 Joints := [op(Joints), struct('parent',  3, 'r', <    0,               ThighLength,                0>, 'e', <     0,     0,     0>)]: #4
;
 Joints := [op(Joints), struct('parent',  4, 'r', <     0,               ShinLength,                0>, 'e', <     0,     0,     0>)]: #5
;
 Joints := [op(Joints), struct('parent',  5, 'r', <              0,               0,                0>, 'e', <  0,     Pi/2,     0>)]: #6
;

# Right Leg
 Joints := [op(Joints), struct('parent',  0, 'r', <-HipRollOffsetX, -HipRollOffsetY,  -HipRollOffsetZ>, 'e', <     0,  Pi/2,     0>)]: #7
;
 Joints := [op(Joints), struct('parent',  7, 'r', <  HipYawOffsetX,               0,                0>, 'e', <     0, -Pi/2,     0>)]: #8
;
 Joints := [op(Joints), struct('parent',  8, 'r', <              0,               0, -HipPitchOffsetZ>, 'e', < -Pi/2,  0,     0>)]: #9
;
 Joints := [op(Joints), struct('parent',  9, 'r', <    0,               ThighLength,                0>, 'e', <     0,     0,     0>)]: #10
;
 Joints := [op(Joints), struct('parent', 10, 'r', <     0,               ShinLength,                0>, 'e', <     0,     0,     0>)]: #11
;
 Joints := [op(Joints), struct('parent', 11, 'r', <              0,               0,                0>, 'e', <    0,     Pi/2,     0>)]: #12
;

# Left arm
 Joints := [op(Joints), struct('parent',  0, 'r', <              0, ShoulderOffsetY,  ShoulderOffsetZ>, 'e', < -Pi/2,     0,     0>)]: #13
;
 Joints := [op(Joints), struct('parent', 13, 'r', <ShoulderOffsetX,               0,                0>, 'e', <  Pi/2,     0,     0>)]: #14
;
 Joints := [op(Joints), struct('parent', 14, 'r', < UpperArmLength,               0,                0>, 'e', <     0,  Pi/2,     0>)]: #15
;
 Joints := [op(Joints), struct('parent', 15, 'r', <              0,               0,                0>, 'e', <     0, -Pi/2,     0>)]: #16
;
 Joints := [op(Joints), struct('parent', 16, 'r', < LowerArmLength,               0,                0>, 'e', <     0,  Pi/2,     0>)]: #17
;

# Right arm
 Joints := [op(Joints), struct('parent',  0, 'r', <              0,-ShoulderOffsetY,  ShoulderOffsetZ>, 'e', < -Pi/2,     0,     0>)]: #18
;
 Joints := [op(Joints), struct('parent', 18, 'r', <ShoulderOffsetX,               0,                0>, 'e', <  Pi/2,     0,     0>)]: #19
;
 Joints := [op(Joints), struct('parent', 19, 'r', < UpperArmLength,               0,                0>, 'e', <     0,  Pi/2,     0>)]: #20
;
 Joints := [op(Joints), struct('parent', 20, 'r', <              0,               0,                0>, 'e', <     0, -Pi/2,     0>)]: #21
;
 Joints := [op(Joints), struct('parent', 21, 'r', < LowerArmLength,               0,                0>, 'e', <     0,  Pi/2,     0>)]: #22
;

# Head
Joints := [op(Joints), struct('parent',  0, 'r', <              0,                0,      NeckOffsetZ>, 'e', <    0,     0,     0>)]: #23
Joints := [op(Joints), struct('parent', 23, 'r', <              0,                0,                0>, 'e', <-Pi/2,     0,     0>)]: #24
Joints := [op(Joints), struct('parent', 24, 'r', <              0,                0,                0>, 'e', <0,     Pi/2,     0>)]: #25

# Torso 
Joints := [op(Joints), struct('parent',  0, 'r', <             0,               0,      TorsoOffsetZ>, 'e', <    0,      0,     0>)]: #26

#==========================================================================================================================

# Definition of end-effectors

# Indexes
Waist := 1:
LLeg  := 2:
RLeg  := 3:
LHand := 4:
RHand := 5:
Head  := 6:
Torso := 7:

EE := [op(EE), struct('parent',  0, 'r', <           0,           0,            0>, 'e', <    0,     0,     0>)]: #Waist
EE := [op(EE), struct('parent',  6, 'r', <  0,           FootHeight,            0>, 'e', <    Pi/2,     0,  Pi/2>)]: #LLeg
EE := [op(EE), struct('parent', 12, 'r', <  0,           FootHeight,            0>, 'e', <    Pi/2,     0,  Pi/2>)]: #RLeg
EE := [op(EE), struct('parent', 17, 'r', <           0,           0,  HandOffsetZ>, 'e', <    0, -Pi/2,     0>)]: #LHand
EE := [op(EE), struct('parent', 22, 'r', <           0,           0,  HandOffsetZ>, 'e', <    0, -Pi/2,     0>)]: #RHand
EE := [op(EE), struct('parent', 25, 'r', <           0,           0,            0>, 'e', < Pi/2,     0,  Pi/2>)]: #Head
EE := [op(EE), struct('parent', 26, 'r', <           0,           0,            0>, 'e', <    0,     0,     0>)]: #Torso

#==========================================================================================================================

# Defines the mass and CoM position (in the WaistFrame) of the Waist link
WaistLink := struct('r', <   -0.00018,    -0.0095,     -0.00018>, 'm', 0.47509): #0

Links := [op(Links), struct('r', <          -0.00018,            -0.0095,           -0.00030>, 'm',  0.12417)]: #1
Links := [op(Links), struct('r', <          -0.00306,            0.00015,           -0.02608>, 'm',  0.06210)]: #2
Links := [op(Links), struct('r', <           0.04627,            0.00033,            0.00010>, 'm',  0.35202)]: #3
Links := [op(Links), struct('r', <           0.04494,            0.00018,                0.0>, 'm',  0.10853)]: #4
Links := [op(Links), struct('r', <          -0.01582,            0.01929,           -0.00016>, 'm',  0.32408)]: #5
Links := [op(Links), struct('r', <           0.05088,            0.00027,           -0.00003>, 'm',  0.50514)]: #6

Links := [op(Links), struct('r', <          -0.00018,             0.0095,           -0.00030>, 'm',  0.12417)]: #7
Links := [op(Links), struct('r', <          -0.00306,            0.00015,           -0.02608>, 'm',  0.06210)]: #8
Links := [op(Links), struct('r', <           0.04627,            0.00033,            0.00010>, 'm',  0.35202)]: #9
Links := [op(Links), struct('r', <           0.04494,            0.00018,                0.0>, 'm',  0.10853)]: #10
Links := [op(Links), struct('r', <          -0.01582,            0.01929,           -0.00016>, 'm',  0.32408)]: #11
Links := [op(Links), struct('r', <           0.05088,           -0.00027,           -0.00003>, 'm',  0.50514)]: #12

Links := [op(Links), struct('r', <               0.0,                0.0,            -0.0345>, 'm',     0.06)]: #13
Links := [op(Links), struct('r', <            0.0558,                0.0,                0.0>, 'm',     0.18)]: #14
Links := [op(Links), struct('r', <               0.0,                0.0,                0.0>, 'm',     0.06)]: #15
Links := [op(Links), struct('r', <            0.0411,                0.0,                0.0>, 'm',     0.18)]: #16
Links := [op(Links), struct('r', <               0.0,                0.0,              0.020>, 'm',     0.06)]: #17

Links := [op(Links), struct('r', <               0.0,                0.0,             0.0345>, 'm',     0.06)]: #18
Links := [op(Links), struct('r', <            0.0558,                0.0,                0.0>, 'm',     0.18)]: #19
Links := [op(Links), struct('r', <               0.0,                0.0,                0.0>, 'm',     0.06)]: #20
Links := [op(Links), struct('r', <            0.0411,                0.0,                0.0>, 'm',     0.18)]: #21
Links := [op(Links), struct('r', <               0.0,                0.0,              0.020>, 'm',     0.06)]: #22

Links := [op(Links), struct('r', <               0.0,                0.0,                0.0>, 'm',     0.06)]: #23
Links := [op(Links), struct('r', <               0.0,                0.0,                0.0>, 'm',     0.24)]: #24
Links := [op(Links), struct('r', <               0.0,                0.0,                0.0>, 'm',     0.12)]: #25

Links := [op(Links), struct('r', <           -0.0241,            -0.0012,             0.0700>, 'm',  0.92465)]: #26
#==========================================================================================================================
;
# 
# 
