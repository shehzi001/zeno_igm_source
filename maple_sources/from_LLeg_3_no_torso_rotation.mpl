
# from LLeg (see Readme_M.txt and JacobianCoM.mpl)

alias(fname = from_LLeg_3_no_Waist_rotation):

#---------------------------------------------------------------------
printf("form output ...\n");
st := time():

# ===================================================================================
# for convenience we use a homogeneous matrix to store the posture of the LLeg
# LL is used as an input parameter in the C function
# ===================================================================================
LL := vector(16): # Left leg transformation matrix <-- this is the support leg

q[30] := LL[1]: q[33] := LL[5]: q[36] :=  LL[9]: q[27] := LL[13]:
q[31] := LL[2]: q[34] := LL[6]: q[37] := LL[10]: q[28] := LL[14]:
q[32] := LL[3]: q[35] := LL[7]: q[38] := LL[11]: q[29] := LL[15]:
#        LL[4]:          LL[8]:          LL[12]:          LL[16]:
# ===================================================================================

# Form Jacobian matrices
J_RL_from_LL := FormJacobian(RLeg,LLeg):
J_Waist_from_LL := FormJacobian(Waist,LLeg):

JointFrames := FramesWorld(LLeg):
CM := SystemCoM(JointFrames):

# Associate an end-effector with the CoM of each link
# (because I don't want to change the function FormJacobianFull)
EE:=EE[1..7]: # leave only the first 7 in the list in case others have been added
EE := [op(EE), struct('parent',  0, 'r', WaistLink:-r, 'e', < 0,0,0>)]:
for i from 1 to n do
    EE := [op(EE), struct('parent',  i, 'r', Links[i]:-r, 'e', < 0,0,0>)]:
end do:

m := 0:
J := Matrix(3,26,0):
for i from 0 to n do
    Je := FormJacobianFull(6+i+1, LLeg):

    if i=0 then
        J := J + WaistLink:-m.Je[1..3,1..n]:
        m := m + WaistLink:-m:
    else
        J := J + Links[i]:-m.Je[1..3,1..n]:
        m := m + Links[i]:-m:
    end if;

end do:
J := J/m: # Jacobian of the CoM

Jac := Matrix(10,12,0):
Jac[1...6,1..12] := J_RL_from_LL:
Jac[7..9,1..12] := J[1..3,1..12]:
Jac[10,1..12] := <0|0|1|0|0|0|0|0|-1|0|0|0>: # constraint q[3] = q[9]

# FGM
RLegT := EEWorld(JointFrames,RLeg):
WaistT := EEWorld(JointFrames,Waist):

RL := vector(16): # Right leg transformation matrix
err1 := FormError(convert(transpose(RLegT),vector), RL):

CoM := vector(3): # CoM position
CM := SystemCoM(JointFrames):
err2 := <CoM[1] - CM[1], CoM[2] - CM[2], CoM[3] - CM[3]>:

out := ArrayTools:-Concatenate(2,convert(transpose(Jac),vector),transpose(err1),transpose(err2),0):

printf("completed in %f seconds\n\n",time()-st);

#---------------------------------------------------------------------

printf("makeproc ...\n");
st := time():

fname := makeproc(out,[q::array(1..n), LL::array(1..linalg:-vectdim(LL)), RL::array(1..linalg:-vectdim(RL)), CoM::array(1..linalg:-vectdim(CoM))]):

printf("completed in %f seconds\n\n",time()-st);

#---------------------------------------------------------------------

printf("Generating %s ...\n",cat(fname,".c"));
st := time():

fd := fopen(cat("../c_sources/",cat(fname,".c")),WRITE):
fprintf(fd,"/* Generated using codegen (%s) */\n", StringTools:-FormatTime("%Y-%m-%d, %T")):
fclose(fd):

C(fname, optimized, filename = cat("../c_sources/",cat(fname,".c")));

printf("completed in %f seconds\n\n",time()-st);

#---------------------------------------------------------------------
# 
